FROM python:3.8.5 AS builder

# Install development headers necessary for building required Python modules.
RUN apt-get update \
	&& apt-get install -y libsasl2-dev libldap2-dev libssl-dev python3-dev

COPY requirements.txt /
COPY Licenses/ /Licenses

# Installed packages go into /build so we can easily pull them out again for
# the final image.
RUN pip install --prefix=/build --no-warn-script-location -r /requirements.txt

FROM postgres:12.3 AS postgres
# Build the main image.
FROM python:3.8.5-slim
ENV PYTHONUNBUFFERED=1

## Install pg_isready
COPY --from=postgres /usr/lib/postgresql/12/ /usr/lib/postgresql/12/
RUN ln -s /usr/lib/postgresql/12/bin/* /usr/local/bin/

RUN apt-get update \
	&& apt-get install -y libpq5 libldap-2.4-2 gettext libgettextpo-dev \
	&& rm -rf /var/lib/apt/lists/*

# Pull built Python modules from the builder image, placing them into the
# PYTHONPATH.
COPY --from=builder /build /usr/local

# Copy project files and set work directory.
WORKDIR /django-project
COPY BBBatScale/ /django-project/
COPY gunicorn-init.py /django-project/gunicorn-init.py

# Copy script to check database connection
COPY docker/common/scripts/await_postgres_connection.py /usr/local/lib/python3.8/await_postgres_connection.py
RUN ln -s /usr/local/lib/python3.8/await_postgres_connection.py /usr/local/bin/await_postgres_connection

# Copy entrypoint
COPY docker/common/scripts/docker-entrypoint.py /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]
