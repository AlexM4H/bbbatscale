# Our Productive Setup 

The following describes the setup at our site at Darmstadt University of Applied Sciences (h_da) powered by BBB@Scale.

If you have any questions which are not answered by this document or [Getting Started Guide ](../GettingStarted.md), feel free to send us a mail at mail@bbbatscale.de.
If you want send us an encrypted email, use the pgp-key under
([PGP-Key](../mailAtBBBatScale.de_public.pgp)).

## Overview of the components

The following overview describes the BBB@Scale, BBB-Worker, BBB-Processing and BBB-Playback components involved in our h_da setup.
![hdarchitecture](/Documentation/hdaarchitecture.png?raw=true 'H_DA Architecture')

- BBB@Scale is deployed in our RedHat-Openshift-Container-Plattform (4PODs) running simultaneously 
- Postgres for BBB@Scale is also running in OCP
- HAProxy Load Balancers provide the entry point to BBB@Scale on OCP
- 69x BBB Server with [bbb-webhooks](https://docs.bigbluebutton.org/dev/webhooks.html) enabled
- 1x BBB Server exclusively for processing the recordings
- 1x BBB Server exclusively for playback of processed recordings

In the following you can find a brief system spec of our virtualized infrastructure followed by physical specs

- BBB-Worker and BBB-Playback Nodes (Virtual Resources)
    - 8 vCPU 
    - 16GB RAM
    - 100GB system disk
    - Ubuntu 16.04 as requested by bbb-install

- BBB-Processing Node (Virtual Resources)
    - 16 VCPU
    - 32GB RAM
    - 100GB system disk
    - 2x NVIDIA V100 GPGPU 

### Physical Hardware Resources used
For design and separation reasons we have implemented different tenants to get the resources from their data-centers separated in their own scheduling domain.

- Tenant1 provided 25x BBB-Worker Nodes
    - KVM-Based Virtualization / shared usage with other VMs
    - 151x Physical CPUs
    - 5TB RAM
- Tenant2 provided 20x BBB-Worker / 1x BBB-Processing
    - KVM-Based Virtualisation / exclusive usage
    - 184x Physical CPUs
    - 2TB RAM
    - 4x NVIDIA V100 GPGPUs
- Tenant3 provided 10 BBB-Worker Nodes 
    - VMWare-Based Virtualisation
- Tenant4 provided 10 BBB-Worker Nodes 
    - VMWare-Based Virtualisation
- Tenant5 (experimental) Offsite 10 BBB-Worker Nodes 
    - AWS XLarge

### Tips for setting up BBB
In this chapter we want to share our experiences in setting up BBB and getting rid of some strange behaivous.
#### The crucial interconnect between STUN / TURN setup and the BBB environment.
We got some problems with errors joining Audio Video for some specialized networks behind firewalls having restrictiv access to external webpages. 
An connecting Apple Ipads in newer IOS versions to BBB.

Here you find attached a setup config for the turn-stun-servers.xml file 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--

BigBlueButton open source conferencing system - http://www.bigbluebutton.org/

Copyright (c) 2012 BigBlueButton Inc. and by respective authors (see below).

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 3.0 of the License, or (at your option) any later
version.

BigBlueButton is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with BigBlueButton; if not, see <http://www.gnu.org/licenses/>.

Setup for Ipad Problems and Firewalling issues 
Important same STUN and TURN Server no other Stun in usage that does not support TURN!

-->
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
            http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
            ">

    <bean id="stun1" class="org.bigbluebutton.web.services.turn.StunServer">
        <constructor-arg index="0" value="stun:turn1-bbb.domain.aa:443"/>
    </bean>

    <bean id="stun2" class="org.bigbluebutton.web.services.turn.StunServer">
        <constructor-arg index="0" value="stun:turn2-bbb.domain.aa:443"/>
    </bean>

    <!--bean id="iceCandidate1" class="org.bigbluebutton.web.services.turn.RemoteIceCandidate">
        <constructor-arg index="0" value="192.168.0.1"/>
    </bean-->

    <!-- Turn servers are configured with a secret that's compatible with
         http://tools.ietf.org/html/draft-uberti-behave-turn-rest-00
         as supported by the coturn and rfc5766-turn-server turn servers -->
    <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="XXXSOMEREALYSECRETTHING"/>
        <constructor-arg index="1" value="turn:turn1-bbb.domain.aa:443?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>
    <bean id="turn1" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="XXXSOMEREALYSECRETTHING"/>
        <constructor-arg index="1" value="turns:turn1-bbb.domain.aa:443?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="turn2" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="XXXSOMEOTHERREALYSECRETTHING"/>
        <constructor-arg index="1" value="turn:turn2-bbb.domain.aa:443?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>
    <bean id="turn3" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="XXXSOMEOTHERREALYSECRETTHING"/>
        <constructor-arg index="1" value="turns:turn2-bbb.domain.aa:443?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="stunTurnService" class="org.bigbluebutton.web.services.turn.StunTurnService">
        <property name="stunServers">
            <set>
                <ref bean="stun1" />
                <ref bean="stun2" />
            </set>
        </property>
        <property name="turnServers">
            <set>
                <ref bean="turn0" />
                <ref bean="turn1" />
                <ref bean="turn2" />
                <ref bean="turn3" />
            </set>
        </property>
        <property name="remoteIceCandidates">
            <set>
                <!--ref bean="iceCandidate1" /-->
                <!--ref bean="iceCandidate2" /-->
            </set>
        </property>
    </bean>
</beans>
```

