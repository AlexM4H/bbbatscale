# encoding: utf-8
import logging
from datetime import timedelta
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from tendo.singleton import SingleInstance, SingleInstanceException
from core.constants import ROOM_STATE_CREATING
from core.models import Room, Server, GeneralParameter
from core.services import reset_room
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all servers'

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start collect_server_stats command ")

        try:
            SingleInstance()
            logger.debug("Start collecting server stats")
            for server in Server.objects.all():
                logger.debug("Call collect_stats on server={}".format(server))
                server.collect_stats()
            logger.debug("Done collecting server stats")

            logger.debug("Start updating max counter")
            # update max counter
            gp = GeneralParameter.load()
            participant_current = Room.get_participants_current()
            logger.debug("participant_current={}; ".format(participant_current) +
                         "participant_total_max={}".format(gp.participant_total_max))

            if participant_current > gp.participant_total_max:
                logger.info("New participant_total_max  ({}) reached".format(participant_current))
                gp.participant_total_max = participant_current
                gp.save()
            logger.debug("Done updating max counter")

            logger.debug("Start reset_room for rooms that were in creating state for more than 60 sec")
            # todo extract to own command
            for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=60),
                                            state=ROOM_STATE_CREATING):
                reset_room(room.meeting_id, room.name, room.config)
                logger.error("Room ({}) was in state 'creating' for more than 30 seconds. Has been reset!".format(room))
            logger.debug("End reset_room")

        except SingleInstanceException as e:
            logger.error("An error occurred during the collection of server stats. Details:" + str(e))

        logger.info("Start collect_server_stats command ")
