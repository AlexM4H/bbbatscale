import pytest
from django.conf import settings
from django.contrib.auth.models import Group, AnonymousUser

from core.models import Tenant, Room, User, get_default_room_config, HomeRoom, \
    PersonalRoom
from core.services import join_or_create_meeting_permissions


@pytest.fixture(scope='function')
def moderator_group(db) -> Group:
    return Group.objects.create(name=settings.MODERATORS_GROUP)


@pytest.fixture(scope='function')
def user_bbb_mod(db, moderator_group) -> User:
    user = User.objects.create_user(
        username="bbb_mod",
        email="bbb_mod@example.org",
        password="bbb_mod"
    )
    user.groups.add(moderator_group)
    return user


@pytest.fixture(scope='function')
def user_bbb_mod2(db, moderator_group) -> User:
    user = User.objects.create_user(
        username="bbb_mod2",
        email="bbb_mod2@example.org",
        password="bbb_mod2"
    )
    user.groups.add(moderator_group)
    return user


@pytest.fixture(scope='function')
def user_no_bbb_mod(db) -> User:
    user = User.objects.create_user(
        username="no_bbb_mod",
        email="no_bbb_mod@example.org",
        password="no_bbb_mod"
    )
    return user


@pytest.fixture(scope='function')
def user_superuser(db) -> User:
    user = User.objects.create(
        username="user_superuser",
        email="user_superuser@example.org",
        is_superuser=True
    )
    return user


@pytest.fixture(scope='function')
def user_ano(db) -> AnonymousUser:
    return AnonymousUser()


@pytest.fixture(scope='function')
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope='function')
def room(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="room_every_one_can_start",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        everyone_can_start=True,
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_auth_needed(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="auth_needed",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        everyone_can_start=False,
        authenticated_user_can_start=True,
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_moderator(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="mod_room",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        everyone_can_start=False,
        authenticated_user_can_start=False,
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_homeroom_mod(db, example, user_bbb_mod) -> HomeRoom:
    return HomeRoom.objects.create(
        tenant=example,
        name="homeroom_mod",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        config=get_default_room_config(),
        owner=user_bbb_mod
    )


@pytest.fixture(scope='function')
def room_personal_mod(db, example, user_bbb_mod) -> PersonalRoom:
    return PersonalRoom.objects.create(
        tenant=example,
        name="room_personal_mod",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        config=get_default_room_config(),
        owner=user_bbb_mod
    )


@pytest.mark.django_db
def test_bbb_mod_and_no_bbb_mod_can_create(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                                           room):
    assert join_or_create_meeting_permissions(user_ano, room)
    assert join_or_create_meeting_permissions(user_no_bbb_mod, room)
    assert join_or_create_meeting_permissions(user_bbb_mod, room)
    assert join_or_create_meeting_permissions(user_bbb_mod2, room)
    assert join_or_create_meeting_permissions(user_superuser, room)


@pytest.mark.django_db
def test_every_authenticated_user_can_start(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                                            room_auth_needed):
    assert not join_or_create_meeting_permissions(user_ano, room_auth_needed)
    assert join_or_create_meeting_permissions(user_no_bbb_mod, room_auth_needed)
    assert join_or_create_meeting_permissions(user_bbb_mod, room_auth_needed)
    assert join_or_create_meeting_permissions(user_bbb_mod2, room_auth_needed)
    assert join_or_create_meeting_permissions(user_superuser, room_auth_needed)


@pytest.mark.django_db
def test_bbb_mod_can_create(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                            room_moderator):
    assert not join_or_create_meeting_permissions(user_ano, room_moderator)
    assert not join_or_create_meeting_permissions(user_no_bbb_mod, room_moderator)
    assert join_or_create_meeting_permissions(user_bbb_mod, room_moderator)
    assert join_or_create_meeting_permissions(user_bbb_mod2, room_moderator)
    assert join_or_create_meeting_permissions(user_superuser, room_moderator)


@pytest.mark.django_db
def test_home_room_permission(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                              room_homeroom_mod):
    assert not join_or_create_meeting_permissions(user_ano, room_homeroom_mod)
    assert not join_or_create_meeting_permissions(user_no_bbb_mod, room_homeroom_mod)
    assert join_or_create_meeting_permissions(user_bbb_mod, room_homeroom_mod)
    assert not join_or_create_meeting_permissions(user_bbb_mod2, room_homeroom_mod)
    assert join_or_create_meeting_permissions(user_superuser, room_homeroom_mod)


@pytest.mark.django_db
def test_personal_room_permission_no_co_owner(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                                              room_personal_mod):
    assert not join_or_create_meeting_permissions(user_ano, room_personal_mod)
    assert not join_or_create_meeting_permissions(user_no_bbb_mod, room_personal_mod)
    assert join_or_create_meeting_permissions(user_bbb_mod, room_personal_mod)
    assert not join_or_create_meeting_permissions(user_bbb_mod2, room_personal_mod)
    assert join_or_create_meeting_permissions(user_superuser, room_personal_mod)


@pytest.mark.django_db
def test_personal_room_permission_co_owner(user_ano, user_no_bbb_mod, user_bbb_mod, user_bbb_mod2, user_superuser,
                                           room_personal_mod):
    room_personal_mod.co_owners.add(user_no_bbb_mod)
    assert not join_or_create_meeting_permissions(user_ano, room_personal_mod)
    assert join_or_create_meeting_permissions(user_no_bbb_mod, room_personal_mod)
    assert join_or_create_meeting_permissions(user_bbb_mod, room_personal_mod)
    assert not join_or_create_meeting_permissions(user_bbb_mod2, room_personal_mod)
    assert join_or_create_meeting_permissions(user_superuser, room_personal_mod)
