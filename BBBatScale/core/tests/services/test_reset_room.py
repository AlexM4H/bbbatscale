import pytest
from core.services import reset_room
from core.models import Room, Tenant, Server


@pytest.fixture(scope='function')
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope='function')
def example_server(db, example) -> Server:
    return Server.objects.create(
        tenant=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope='function')
def room_d14_0303(db, example, example_server) -> Room:
    return Room.objects.create(
        tenant=example,
        server=example_server,
        meeting_id="12345678",
        name="D14/03.03",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
        config=None,
    )


@pytest.fixture(scope='function')
def room_d14_0301(db, example, example_server) -> Room:
    return Room.objects.create(
        tenant=example,
        server=example_server,
        meeting_id="87654321",
        name="D14/03.01",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
    )


def test_reset_room(room_d14_0303, room_d14_0301):
    # reset with config
    reset_room(room_d14_0303.meeting_id, room_d14_0303.name,
               room_d14_0303.config)
    # reset without config
    reset_room(room_d14_0301.meeting_id, room_d14_0301.name,
               room_d14_0301.config)

    server = Room.objects.get(meeting_id=room_d14_0303.meeting_id).server
    state = Room.objects.get(meeting_id=room_d14_0303.meeting_id).state
    participant_count = Room.objects.get(meeting_id=room_d14_0303.meeting_id).participant_count
    videostream_count = Room.objects.get(meeting_id=room_d14_0303.meeting_id).videostream_count
    last_running = Room.objects.get(meeting_id=room_d14_0303.meeting_id).last_running
    mute_on_start = Room.objects.get(meeting_id=room_d14_0303.meeting_id).mute_on_start
    all_moderator = Room.objects.get(meeting_id=room_d14_0303.meeting_id).all_moderator
    guest_policy = Room.objects.get(meeting_id=room_d14_0303.meeting_id).guest_policy
    allow_guest_entry = Room.objects.get(meeting_id=room_d14_0303.meeting_id).allow_guest_entry
    access_code = Room.objects.get(meeting_id=room_d14_0303.meeting_id).access_code
    access_code_guests = Room.objects.get(meeting_id=room_d14_0303.meeting_id).access_code_guests
    disable_cam = Room.objects.get(meeting_id=room_d14_0303.meeting_id).disable_cam
    disable_mic = Room.objects.get(meeting_id=room_d14_0303.meeting_id).disable_mic
    disable_note = Room.objects.get(meeting_id=room_d14_0303.meeting_id).disable_note
    disable_private_chat = Room.objects.get(meeting_id=room_d14_0303.meeting_id).disable_private_chat
    disable_public_chat = Room.objects.get(meeting_id=room_d14_0303.meeting_id).disable_public_chat
    allow_recording = Room.objects.get(meeting_id=room_d14_0303.meeting_id).allow_recording
    url = Room.objects.get(meeting_id=room_d14_0303.meeting_id).url
    logoutUrl = Room.objects.get(meeting_id=room_d14_0303.meeting_id).logoutUrl
    welcome_message = Room.objects.get(meeting_id=room_d14_0303.meeting_id).welcome_message
    dialNumber = Room.objects.get(meeting_id=room_d14_0303.meeting_id).dialNumber

    assert server is None
    assert state == "INACTIVE"
    assert participant_count == 0
    assert videostream_count == 0
    assert last_running is None
    assert mute_on_start
    assert all_moderator is False
    assert guest_policy == "ALWAYS_ACCEPT"
    assert allow_guest_entry is False
    assert access_code is None
    assert access_code_guests is None
    assert disable_cam is False
    assert disable_mic is False
    assert disable_note is False
    assert disable_private_chat is False
    assert disable_public_chat is False
    assert allow_recording is False
    assert url is None
    assert logoutUrl is None
    assert welcome_message is None
    assert dialNumber is None

    server = Room.objects.get(meeting_id=room_d14_0301.meeting_id).server
    participant_count = Room.objects.get(meeting_id=room_d14_0301.meeting_id).participant_count
    state = Room.objects.get(meeting_id=room_d14_0301.meeting_id).state
    videostream_count = Room.objects.get(meeting_id=room_d14_0301.meeting_id).videostream_count
    last_running = Room.objects.get(meeting_id=room_d14_0301.meeting_id).last_running
    assert participant_count == 0
    assert server is None
    assert state == "INACTIVE"
    assert videostream_count == 0
    assert last_running is None
