import pytest
from core.services import get_rooms_with_current_next_event
from freezegun import freeze_time
from datetime import datetime, timezone
from core.models import Room, RoomEvent, Tenant, get_default_room_config


@pytest.fixture(scope='function')
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope='function')
def room_d14_0204(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        name="D14/02.04",
        config=get_default_room_config()
    )


@pytest.fixture(scope='function')
def room_d14_0204_event_one(db, room_d14_0204) -> RoomEvent:
    return RoomEvent.objects.create(
        uid="Crypto",
        room=room_d14_0204,
        name="Crypto",
        start=datetime(2020, 5, 25, 8, 30, tzinfo=timezone.utc),
        end=datetime(2020, 5, 25, 10, 00, tzinfo=timezone.utc)
    )


@pytest.fixture(scope='function')
def room_d14_0204_event_two(db, room_d14_0204) -> RoomEvent:
    return RoomEvent.objects.create(
        uid="DB2",
        room=room_d14_0204,
        name="DB2",
        start=datetime(2020, 5, 25, 10, 15, tzinfo=timezone.utc),
        end=datetime(2020, 5, 25, 11, 45, tzinfo=timezone.utc)
    )


@pytest.fixture(scope='function')
def room_d14_0204_event_three(db, room_d14_0204) -> RoomEvent:
    return RoomEvent.objects.create(
        uid="OOAD",
        room=room_d14_0204,
        name="OOAD",
        start=datetime(2020, 5, 25, 14, 15, tzinfo=timezone.utc),
        end=datetime(2020, 5, 25, 15, 45, tzinfo=timezone.utc)
    )


@freeze_time("2020-05-25 06:30:00", tz_offset=0)
def test_get_rooms_with_current_next_event_without_current_with_next(room_d14_0204_event_one, room_d14_0204_event_two,
                                                                     room_d14_0204_event_three):
    rooms = get_rooms_with_current_next_event()
    assert rooms[0].room_occupancy_current is None
    assert rooms[0].room_occupancy_next == "Crypto"


@freeze_time("2020-05-25 08:30:00", tz_offset=0)
def test_get_rooms_with_current_next_event_with_current_and_next1(room_d14_0204_event_one,
                                                                  room_d14_0204_event_two,
                                                                  room_d14_0204_event_three):
    rooms = get_rooms_with_current_next_event()
    assert rooms[0].room_occupancy_current == "Crypto"
    assert rooms[0].room_occupancy_next == "DB2"


@freeze_time("2020-05-25 15:30:00", tz_offset=0)
def test_get_rooms_with_current_next_event_with_current_and_next2(room_d14_0204_event_one,
                                                                  room_d14_0204_event_two,
                                                                  room_d14_0204_event_three):
    rooms = get_rooms_with_current_next_event()
    assert rooms[0].room_occupancy_current == "OOAD"
    assert rooms[0].room_occupancy_next is None


@freeze_time("2020-05-25 16:30:00", tz_offset=0)
def test_get_rooms_with_current_next_event_with_current_and_next3(room_d14_0204_event_one,
                                                                  room_d14_0204_event_two,
                                                                  room_d14_0204_event_three):
    rooms = get_rooms_with_current_next_event()
    assert rooms[0].room_occupancy_current is None
    assert rooms[0].room_occupancy_next is None
