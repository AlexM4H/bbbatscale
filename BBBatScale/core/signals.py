import hashlib
import logging
import uuid

from django.conf import settings
from django.db import IntegrityError
from django.db.models.signals import post_save, pre_save
from django.dispatch.dispatcher import receiver
from django.utils.crypto import get_random_string

from core.models import Room, HomeRoom, GeneralParameter, User, PersonalRoom, get_default_room_config, ExternalRoom

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Room)
@receiver(post_save, sender=HomeRoom)
@receiver(post_save, sender=PersonalRoom)
def set_values_after_room_creation(instance, created, *args, **kwargs):
    logger.info("Signal '%s' started", "set_values_after_room_creation")
    if created:
        logger.debug("create event for room={}".format(instance))
        if not instance.meeting_id:
            instance.meeting_id = uuid.uuid4().hex
            logger.debug("meeting_id set to {}".format(instance.meeting_id))
        if not instance.attendee_pw:
            instance.attendee_pw = get_random_string(length=10)
            logger.debug("attendee_pw was updated")
        if not instance.moderator_pw:
            instance.moderator_pw = get_random_string(length=10)
            logger.debug("moderator_pw was updated")
        instance.save()


@receiver(pre_save, sender=Room)
@receiver(pre_save, sender=ExternalRoom)
@receiver(pre_save, sender=HomeRoom)
@receiver(pre_save, sender=PersonalRoom)
def set_default_config(sender, instance: Room, raw, using, update_fields, *args, **kwargs):
    logger.info("Signal '%s' started", "set_default_config")
    if instance.config_id is None:
        instance.config_id = get_default_room_config().pk
        logger.debug("Set config_id={} ({}) ".format(instance.config_id, get_default_room_config()) +
                     "for room={}".format(instance))


@receiver(post_save, sender=User)
def create_home_room_on_user_creation(instance: User, created, *args, **kwargs):
    logger.info("Signal '%s' started", "create_home_room_on_user_creation")
    get_or_create_home_room_for_user(instance)


@receiver(post_save, sender=GeneralParameter)
def update_home_room_on_general_parameter_change(instance: GeneralParameter, created, *args, **kwargs):
    logger.info("Signal '%s' started", "update_home_room_on_general_parameter_change")
    if instance.home_room_enabled:
        if instance.home_room_tenant is None or instance.home_room_room_configuration is None:
            HomeRoom.objects.all().delete()
            msg = 'Misconfiguration! home_room_enabled has been set to {} '.format(instance.home_room_enabled) + \
                  'but home_room_tenant(={}) or '.format(instance.home_room_tenant) + \
                  'home_room_room_configuration(={}) is unset'.format(instance.home_room_room_configuration)
            logger.warning(msg)
        else:
            if instance.home_room_teachers_only:
                HomeRoom.objects.exclude(owner__groups__name__contains=settings.MODERATORS_GROUP).delete()
                logger.debug("home_room_teachers_only is set thus, home rooms of moderators have been deleted")

            HomeRoom.objects.all().update(tenant=instance.home_room_tenant,
                                          config=instance.home_room_room_configuration)
            logger.debug("All home rooms have been set to tenant=%s and room_config=%s",
                         instance.home_room_tenant,
                         instance.home_room_room_configuration)

            for user in User.objects.all():
                get_or_create_home_room_for_user(user)

    elif not instance.home_room_enabled:
        HomeRoom.objects.all().delete()


def get_or_create_home_room_for_user(user: User):
    logger.debug("Create homeroom for user %s", user)

    def create_room_name(user: User):
        if user.email and not HomeRoom.objects.filter(name="home-" + user.email).exists():
            name = "home-" + user.email
        else:
            name = "home-" + user.last_name + "-" + hashlib.sha1(bytes(user.username, "UTF-8")).hexdigest()[:8]
        logger.debug("Generated home room name for user %s is %s", user.username, name)
        return name

    general_parameters: GeneralParameter = GeneralParameter.load()
    user_is_teacher = user.groups.filter(name__icontains=settings.MODERATORS_GROUP).exists() or user.is_superuser

    if general_parameters.home_room_enabled and user_is_teacher or \
            not general_parameters.home_room_teachers_only and not user_is_teacher:

        try:
            room_name = create_room_name(user)
            HomeRoom.objects.get_or_create(
                name=room_name,
                owner=user,
                tenant=general_parameters.home_room_tenant,
                is_public=False,
                config=general_parameters.home_room_room_configuration
            )
            logger.debug("Home room created with the following parameters: " +
                         "name=% owner=% tenant=% is_public=% config = %s",
                         room_name, user, general_parameters.home_room_tenant, False,
                         general_parameters.home_room_room_configuration)
        except IntegrityError as e:
            logger.warning("Home room could not be created. " + str(e))
