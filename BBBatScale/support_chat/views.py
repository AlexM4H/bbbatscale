import json

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import DataError
from django.http import HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render, redirect

from support_chat.models import SupportChatParameter, Supporter, PreparedAnswer
from support_chat.utils import has_supporter_privileges


@login_required
@user_passes_test(has_supporter_privileges)
def chats_overview(request) -> HttpResponse:
    prepared_answers = [
        {"title": prepared_answer.title, "text": prepared_answer.text}
        for prepared_answer in PreparedAnswer.objects.all()
    ]

    return render(request, "support_chat/chats_overview.html", {"prepared_answers": json.dumps(prepared_answers)})


@login_required
@user_passes_test(has_supporter_privileges)
def prepared_answers_overview(request) -> HttpResponse:
    if request.method == "GET":
        return render(
            request,
            "support_chat/prepared_answers_overview.html",
            {"prepared_answers": list(PreparedAnswer.objects.all())},
        )
    elif request.method == "POST":
        try:
            if "id" in request.POST:
                prepared_answers = PreparedAnswer.objects.get(id=int(request.POST["id"]))

                if "title" in request.POST and "text" in request.POST:
                    prepared_answers.title = request.POST["title"]
                    prepared_answers.text = request.POST["text"]
                    prepared_answers.save()
                else:
                    prepared_answers.delete()
            else:
                PreparedAnswer.objects.create(title=request.POST["title"], text=request.POST["text"])
        except (ValueError, KeyError, PreparedAnswer.DoesNotExist, DataError):
            pass
        return redirect("support_chat_prepared_answers_overview")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])


@login_required
@user_passes_test(has_supporter_privileges)
def supporters_overview(request) -> HttpResponse:
    active_supporters = []
    inactive_supporters = []
    for supporter in Supporter.objects.prefetch_related("id").all():
        if supporter.is_active:
            active_supporters.append(str(supporter.user))
        else:
            inactive_supporters.append(str(supporter.user))

    return render(
        request,
        "support_chat/supporters_overview.html",
        {"active_supporters": active_supporters, "inactive_supporters": inactive_supporters},
    )


@login_required
@staff_member_required
def settings(request) -> HttpResponse:
    if request.method == "GET":
        return render(
            request,
            "support_chat/settings.html",
            {"disable_chat_if_offline": SupportChatParameter.load().disable_chat_if_offline},
        )
    elif request.method == "POST":
        try:
            support_chat_parameters = SupportChatParameter.load()
            support_chat_parameters.disable_chat_if_offline = "disableChatIfOffline" in request.POST
            support_chat_parameters.message_max_length = int(request.POST["messageMaxLength"])
            support_chat_parameters.save()
        except (ValueError, KeyError):
            pass
        return redirect("support_chat_settings")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])
