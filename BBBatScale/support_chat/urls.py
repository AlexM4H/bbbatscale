from django.urls import path
from django.views.i18n import JavaScriptCatalog

from support_chat import views

urlpatterns = [
    path("jsi18n", JavaScriptCatalog.as_view(packages=["support_chat"]), name="support_chat_javascript_catalog"),
    path("chats", views.chats_overview, name="support_chat_chats_overview"),
    path("prepared-answers", views.prepared_answers_overview, name="support_chat_prepared_answers_overview"),
    path("supporters", views.supporters_overview, name="support_chat_supporters_overview"),
    path("settings", views.settings, name="support_chat_settings"),
]
