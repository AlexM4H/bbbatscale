import os

os.environ.setdefault("DJANGO_SECRET_KEY", "DUMMY_DJANGO_SECRET_KEY")
os.environ.setdefault("BASE_URL", "http://localhost:8000")
os.environ.setdefault("RECORDINGS_SECRET", "DUMMY_RECORDINGS_SECRET")
os.environ.setdefault("POSTGRES_DB", "bbbatscale")
os.environ.setdefault("POSTGRES_USER", "bbbatscale")
os.environ.setdefault("POSTGRES_PASSWORD", "DUMMY_POSTGRES_PASSWORD")
os.environ.setdefault("POSTGRES_HOST", "localhost")
os.environ.setdefault("REDIS_HOST", "localhost")
os.environ.setdefault("SUPPORT_CHAT", "enabled")
os.environ.setdefault("WEBHOOKS", "enabled")

from .base import *  # noqa: F401,F403,E402

DEBUG = True
